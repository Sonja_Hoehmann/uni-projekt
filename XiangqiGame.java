package de.tuberlin.sese.swtpp.gameserver.model.xiangqi;

import de.tuberlin.sese.swtpp.gameserver.model.*;

//TODO: more imports from JVM allowed here
import java.text.StringCharacterIterator;
import java.text.CharacterIterator;
import java.util.*;
//

import java.io.Serializable;

public class XiangqiGame extends Game implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 5424778147226994452L;

	/************************
	 * member
	 ***********************/

	// just for better comprehensibility of the code: assign red and black player
	private Player blackPlayer;
	private Player redPlayer;

	// internal representation of the game state
	// TODO: insert additional game data here
	public String realBoard = "rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR";
	//public String realBoard = "rheagae1r/9/1c5c1/s1sh3ss/9/9/S1S1H1S1S/1C5C1/9/RHEAGAE1R";
	public Game game;
	public Move move;
	
	/************************
	 * constructors
	 ***********************/

	public XiangqiGame() {
		super();

		// TODO: initialization of game state can go here
	}

	public String getType() {
		return "xiangqi";
	}

	/*******************************************
	 * Game class functions already implemented
	 ******************************************/

	@Override
	public boolean addPlayer(Player player) {
		if (!started) {
			players.add(player);

			// game starts with two players
			if (players.size() == 2) {
				started = true;
				this.redPlayer = players.get(0);
				this.blackPlayer = players.get(1);
				nextPlayer = redPlayer;
			}
			return true;
		}

		return false;
	}

	@Override
	public String getStatus() {
		if (error)
			return "Error";
		if (!started)
			return "Wait";
		if (!finished)
			return "Started";
		if (surrendered)
			return "Surrendered";
		if (draw)
			return "Draw";

		return "Finished";
	}

	@Override
	public String gameInfo() {
		String gameInfo = "";

		if (started) {
			if (blackGaveUp())
				gameInfo = "black gave up";
			else if (redGaveUp())
				gameInfo = "red gave up";
			else if (didRedDraw() && !didBlackDraw())
				gameInfo = "red called draw";
			else if (!didRedDraw() && didBlackDraw())
				gameInfo = "black called draw";
			else if (draw)
				gameInfo = "draw game";
			else if (finished)
				gameInfo = blackPlayer.isWinner() ? "black won" : "red won";
		}

		return gameInfo;
	}

	@Override
	public String nextPlayerString() {
		return isRedNext() ? "r" : "b";
	}

	@Override
	public int getMinPlayers() {
		return 2;
	}

	@Override
	public int getMaxPlayers() {
		return 2;
	}

	@Override
	public boolean callDraw(Player player) {

		// save to status: player wants to call draw
		if (this.started && !this.finished) {
			player.requestDraw();
		} else {
			return false;
		}

		// if both agreed on draw:
		// game is over
		if (players.stream().allMatch(Player::requestedDraw)) {
			this.draw = true;
			finish();
		}
		return true;
	}

	@Override
	public boolean giveUp(Player player) {
		if (started && !finished) {
			if (this.redPlayer == player) {
				redPlayer.surrender();
				blackPlayer.setWinner();
			}
			if (this.blackPlayer == player) {
				blackPlayer.surrender();
				redPlayer.setWinner();
			}
			surrendered = true;
			finish();

			return true;
		}

		return false;
	}

	/*
	 * ****************************************** Helpful stuff
	 */

	/**
	 *
	 * @return True if it's red player's turn
	 */
	public boolean isRedNext() {
		return nextPlayer == redPlayer;
	}

	/**
	 * Ends game after regular move (save winner, finish up game state,
	 * histories...)
	 *
	 * @param winner player who won the game
	 * @return true if game was indeed finished
	 */
	public boolean regularGameEnd(Player winner) {
		// public for tests
		if (finish()) {
			winner.setWinner();
			winner.getUser().updateStatistics();
			return true;
		}
		return false;
	}

	public boolean didRedDraw() {
		return redPlayer.requestedDraw();
	}

	public boolean didBlackDraw() {
		return blackPlayer.requestedDraw();
	}

	public boolean redGaveUp() {
		return redPlayer.surrendered();
	}

	public boolean blackGaveUp() {
		return blackPlayer.surrendered();
	}

	/*******************************************
	 * !!!!!!!!! To be implemented !!!!!!!!!!!!
	 ******************************************/

	@Override
	public void setBoard(String state) {
		// Note: This method is for automatic testing. A regular game would not start at
		// some artificial state.
		// It can be assumed that the state supplied is a regular board that can be
		// reached during a game.
		// TODO: implement
		//board von letztem Move in gameHistory festsetzen
		
		realBoard = state;
		
	}

	@Override
	public String getBoard() {
		// TODO: implement
		//board von letztem Move in gameHistory holen
		return realBoard;
	}

	
	public boolean tryMoveRot(String moveString) {
		String boardDavor = realBoard;
		setBoard(configureNewBoard(moveString));
		String schach1 = Schach(findeGeneralFeld('g'), "r");
		String schach2 = Schach(findeGeneralFeld('G'), "s");
		if(schach1 != "") {
			setNextPlayer(blackPlayer);
			if(istSchachMatt(schach1,findeGeneralFeld('g'), "s") && istSchachMattX(schach1,findeGeneralFeld('g'), "s")) {
				regularGameEnd(redPlayer);
			}
		}
		else if(schach2 != "") {
			setBoard(boardDavor);
			return false;
		}
		setNextPlayer(blackPlayer);
		return true;
	}
	
	public boolean tryMoveSchwarz(String moveString) {
		String boardDavor = realBoard;
		setBoard(configureNewBoard(moveString));
		String schach1 = Schach(findeGeneralFeld('g'), "r");
		String schach2 = Schach(findeGeneralFeld('G'), "s");
		if(schach1 != "") {
			setBoard(boardDavor);
			return false;
		}
		else if(schach2 != "") {
			setNextPlayer(redPlayer);
			if(istSchachMatt(schach2,findeGeneralFeld('G'), "r") && istSchachMattX(schach2,findeGeneralFeld('G'), "r")) {
				regularGameEnd(blackPlayer);
			}
		}
		setNextPlayer(redPlayer);
		return true;
	}
	
	
	@Override
	public boolean tryMove(String moveString, Player player) {
		if (nextPlayer != player) {
			return false;
		}
		String spieler;
		if(isRedNext()) {spieler = "r";}
		else {spieler = "s";}
		
		if(ismovevalid(moveString, spieler) && isRedNext() && tryMoveRot(moveString) 
				|| ismovevalid(moveString, spieler) && isRedNext() == false && tryMoveSchwarz(moveString)) {
			move = new Move(moveString, realBoard, player);
			List<Move> neuHistory = new LinkedList<>();
			if(this.getHistory().isEmpty()) {
				neuHistory.add(move);
				this.setHistory(neuHistory);
			}
			else {this.getHistory().add(move);}
			return true;
		}
		return false;	
		
	}
	

	

	/**
	 * configures the new board after the move was done
	 * 
	 * @param moveString move to be done
	 * @return new board
	 */
	private String configureNewBoard(String moveString) {
		String board = getBoard();
		String[] rowsOfBoard = board.split("/");
		char figur = getfigur(moveString);

		int startColumn = getColumn(moveString.charAt(0));
        int startRow = getrow(moveString.charAt(1));
        int movedColumn = getColumn(moveString.charAt(3));
        int movedRow = getrow(moveString.charAt(4));

        // delete figur from row
        String previousRow = rowsOfBoard[startRow];
        rowsOfBoard[startRow] = removeFigur(previousRow, figur, startColumn);

        // add figur to new row
        String newRow = rowsOfBoard[movedRow];
        rowsOfBoard[movedRow] = placeFigur(newRow, figur, movedColumn);

        return rowsOfBoard[0] + "/" + rowsOfBoard[1] + "/" + rowsOfBoard[2] + "/" + rowsOfBoard[3] + "/"
                + rowsOfBoard[4] + "/" + rowsOfBoard[5] + "/" + rowsOfBoard[6] + "/" + rowsOfBoard[7] + "/"
                + rowsOfBoard[8] + "/" + rowsOfBoard[9];
    }

    /**
     * Helper method of configureNewBoard()
     * @return row where the figur is already placed
     */
    private String placeFigur(String newRow, char figur, int column) {
        LinkedList<Character> asLL = rowToLinkedList(newRow);
        asLL.set(column, figur);
        return llToString(asLL);
    }

    /**
     * Helper method of configureNewBoard()
     * @return row where the figur has left
     */
    private String removeFigur(String previousRow, char figur, int column) {
        LinkedList<Character> asLL = rowToLinkedList(previousRow);
        asLL.set(column, '0');
        return llToString(asLL);
    }

    /**
     * converts Linked List representation of row back to String
     * @param row
     * @return
     */
    private String llToString(LinkedList<Character> row) {
        StringBuilder asString = new StringBuilder();
        int number = 0;
        for (Character character : row) {
            if (Character.isLetter(character)) {
                if (number > 0) {
                    asString.append(Character.forDigit(number, 10));
                    number = 0;
                }
                	asString.append(character);
            } else {
                number++;
            }
        }
        if(number != 0) {
        	 asString.append(Character.forDigit(number, 10));
        }
        return asString.toString();
    }

    /**
     * turn String representation of row into a Linked List
     * Letter indicates a figure, 0 an empty field
     * @param row
     * @return
     */
    private LinkedList<Character> rowToLinkedList(String row) {
        LinkedList<Character> asLL = new LinkedList<>();
        for(int i = 0; i < row.length(); i++) {
            if (Character.isLetter(row.charAt(i))) {
                asLL.add(row.charAt(i));
            }
            else {
                for (int j = 0; j < Character.getNumericValue(row.charAt(i)); j++) {
                    asLL.add('0');
                }
            }
        }
        return asLL;
    }

    /**
     * Helper method of configureNewBoard()
     *
     * @param column letter of column
     * @return responding index of letter
     */
    private int getColumn(char column) {
        String order = "abcdefghi";
        return order.indexOf(column);
    }

    private int getrow(char row) {
        String order = "9876543210";
        return order.indexOf(row);
    }

	/**
	 * Funktion returned char von Figur, sonst 0
	 * 
	 * @param moveString
	 * @return
	 */
    

	
	//muss alle string player probably umschreiben in player player
	public boolean ismovevalid(String moveString, String player) {
		
        char figur = getfigur(moveString);
        if (Character.isDigit(figur) || figur == 'f') {
            return false;
        }
      
        else if(Character.isUpperCase(figur) && player == "r" || Character.isLowerCase(figur) && player == "s"){
            String figurAlsString = figur+"";
            String figurGross = figurAlsString.toUpperCase();
            switch(figurGross){ 
            case "G": return ismovevalidG(moveString, player);
            case "A": return ismovevalidA(moveString, player);
            case "E": return ismovevalidE(moveString, player);
            case "H": return ismovevalidH(moveString, player);
            case "R": return ismovevalidR(moveString, player);
            case "C": return ismovevalidC(moveString, player);
            case "S": return ismovevalidS(moveString, player);
            }
        }
        return false;
    }
//todesblick darf auch nicht entstehen, wenn andere figuren sich wegbewegen wollen

	public boolean zeichen(String moveString) {
        if((int) moveString.charAt(3) > 64 && 
        		(int) moveString.charAt(3) < 91 
        		|| (int) moveString.charAt(3) > 96 && 
        		(int) moveString.charAt(3) < 106){
            if((int) moveString.charAt(1) > 47 &&
            		(int) moveString.charAt(1) < 58 && 
            		(int) moveString.charAt(4) > 47 && 
            		(int) moveString.charAt(4) < 58){
                if((int) moveString.charAt(2) == 45){
                    return true;
                }
            }
        }  
		return false;
	}
	
    public boolean moveStringValid(String moveString){
        if(moveString.length() != 5){
            return false;
        }
        else if((int) moveString.charAt(0) > 64 && (int) moveString.charAt(0) < 91 
        		|| (int) moveString.charAt(0) > 96 && (int) moveString.charAt(0) < 106){
        	  return zeichen(moveString);
        }
      return false;
    }
 
    public String oben(String moveString, String player){
        if(moveString == ""){return "";}
        StringBuilder str = new StringBuilder(moveString);
        str.setCharAt(1,(char)((int) moveString.charAt(1)+1));
        if(moveStringValid(str.toString())){
            return str.toString();
        }
        return "";
    }
    public String unten(String moveString, String player){
        if(moveString == ""){return "";}
        StringBuilder str = new StringBuilder(moveString);
        str.setCharAt(1,(char)((int) moveString.charAt(1)-1));
        if(moveStringValid(str.toString())){
            return str.toString();
        }
        return "";
    }
    public String links(String moveString,String player){
        if(moveString == ""){return "";}
        StringBuilder str = new StringBuilder(moveString);
        str.setCharAt(0,(char)((int) moveString.charAt(0)-1));
        if(moveStringValid(str.toString())){
            return str.toString();
        }
        return "";
    }
    public String rechts(String moveString,String player){
        if(moveString == ""){return "";}
        StringBuilder str = new StringBuilder(moveString);
        str.setCharAt(0,(char)((int) moveString.charAt(0)+1));
        if(moveStringValid(str.toString())){ 
            return str.toString();
        }
        return "";
    }
    public boolean richtig(String neu, String moveString,String player){
        if(neu == ""){return false;}
        if((int) neu.charAt(0) == (int) moveString.charAt(3) && (int) neu.charAt(1) == (int) moveString.charAt(4)){
            if(player == "r" && Character.isLowerCase(getfigur(neu)) ||  player == "s" && Character.isUpperCase(getfigur(neu)) || Character.isDigit(getfigur(neu))){
                return true;
            }
        }
        return false;
    }
    public boolean figurImWeg(String temp, String moveString,String player){
        if(temp == ""){
        	return false;} //false oder true oder was returnen?
        if(Character.isLetter(getfigur(temp)) && richtig(temp,moveString,player) == false){
            return true;
        }
        return false;
    }
   
    //aufgabenstellung: todesblick wirkt wie schach. heißt dass man darf trz irgendwie vom gegner zulassen? 
    public boolean todesblickG(String moveString, String player){
    	char figur;
        String temp2 = String.valueOf( (char) moveString.charAt(3)) + (char)moveString.charAt(4) + (char)(moveString.charAt(2))+(char)(moveString.charAt(0))+(char)(moveString.charAt(1));
       	while(temp2 != "") {
    		if(getfigur(moveString ) == 'G') {
    			temp2 = oben(temp2,player);
    			figur = getfigur(temp2);
    			if(Character.isLetter(figur) && figur != 'g') {return false;}
    			else if(Character.isLetter(figur) && figur == 'g') {break;}
    		}
    		else if(getfigur(moveString)  == 'g') {
    			temp2 = unten(temp2,player);
    			figur = getfigur(temp2);
    			if(Character.isLetter(figur) && figur != 'G') {return false;}
    			else if(Character.isLetter(figur) && figur == 'G') {break;}
    		}	
        } return true;
    }
    
 	//todesblick ist das problem mit bewegen wenn ich als erster drin stehe
    public boolean todesblick(String moveString, String player){
    	String temp = moveString;
    	char figur;
    	while (temp != ""){
             temp = oben(temp,player);
             figur = getfigur(temp);
             if(Character.isLetter(figur) && figur != 'g'){return false;}
             else if(figur == 'g'){
                 while(temp != ""){ 
                	  temp = unten(temp,player);
                     figur = getfigur(temp);
                     //wenn zb die canone angreifen will
                     if(Character.isLetter(figur) && figur != 'G' && (char)temp.charAt(0) == (char) moveString.charAt(3)){return false;}  //  && (char)temp.charAt(1) != (char) moveString.charAt(1)
                     //für ein horse o.ä, wenn noch andere figuren unterhalb sind
                     else if(Character.isLetter(figur) && figur != 'G' && (char)temp.charAt(1) != (char) moveString.charAt(1)) {
                    	 return false;
                     }
                     else if(figur == 'G'){
                    	 return true;
                     }
                 }
             }  
         }
    	return false;
    }
    
    public boolean ismovevalidG(String moveString, String player) {
    	if(todesblickG(moveString, player)){return false;}
        if(player == "r" && (int) moveString.charAt(4) <= 50 && (int) moveString.charAt(3) <= 102 && (int) moveString.charAt(3) >= 100 || player == "s" && (int) moveString.charAt(4) >= 55&& (int) moveString.charAt(3) <= 102 && (int) moveString.charAt(3) >= 100){
            if(richtig(oben(moveString, player), moveString,player)){return true;}
            else if(richtig(unten(moveString,player), moveString,player)){return true;}
            else if(richtig(links(moveString,player), moveString,player)){return true;}
            else if(richtig(rechts(moveString,player), moveString,player)){return true;}
        }
        return false;
    }
    
    public boolean ismovevalidA(String moveString, String player) {
        if(todesblick(moveString, player)){return false;}
        if((player == "r" && (int) moveString.charAt(4) <= 50 && (int) moveString.charAt(3) <= 102 && (int) moveString.charAt(3) >= 100) || player == "s" && (int) moveString.charAt(4) >= 55 && (int) moveString.charAt(3) <= 102 && (int) moveString.charAt(3) >= 100){
            if(richtig(oben(links(moveString,player),player), moveString,player)){return true;}
            else if(richtig(oben(rechts(moveString,player),player), moveString,player)){return true;}
            else if(richtig(unten(links(moveString,player),player), moveString,player)){return true;}
            else if(richtig(unten(rechts(moveString,player),player), moveString,player)){return true;}
        }
        return false;
    }
 
    public boolean breakPoint(String temp, String moveString, String player) {
    	   if(temp == ""){
    		   return true;
    	   }
    	   else if(figurImWeg(temp,moveString,player)){
        	  return true;
        	   }
    	return false;
    }
    
    public boolean funkEObenLinks(String temp, String moveString, String player) {
    	 for (int i = 0; i < 2; i++){
             temp = oben(links(temp,player),player);
             if(breakPoint(temp,moveString,player) == true) {break;}
         }
    	 if(richtig(temp, moveString,player)){
         	return true;
         }
    	return false;
    }
    
    public boolean funkEObenRechts(String temp, String moveString, String player) {
   	 for (int i = 0; i < 2; i++){
            temp = oben(rechts(temp,player),player);
            if(breakPoint(temp,moveString,player) == true) {break;}
        }
   	 if(richtig(temp, moveString,player)){
        	return true;
        }
   	 return false;
    }
    
    public boolean funkEUntenLinks(String temp, String moveString, String player) {
   	 for (int i = 0; i < 2; i++){
            temp = unten(links(temp,player),player);
            if(breakPoint(temp,moveString,player) == true) {break;}
        }
   	 if(richtig(temp, moveString,player)){
        	return true;
        }
   	 return false;
    }
    
    public boolean funkEUntenRechts(String temp, String moveString, String player) {
   	 for (int i = 0; i < 2; i++){
            temp = unten(rechts(temp,player),player);
            if(breakPoint(temp,moveString,player) == true) {break;}
        }
   	 if(richtig(temp, moveString,player)){
        	return true;
        }
   	 return false;
    }
    
    public boolean ismovevalidE(String moveString, String player) {
        if(todesblick(moveString, player) == false && player == "r" && (int) moveString.charAt(4) <= 52 || player == "s" && (int) moveString.charAt(4) >= 53){
            String temp = moveString;
            if(funkEObenLinks(temp,moveString,player)) {return true;}
            temp = moveString;
            if(funkEObenRechts(temp,moveString,player)) {return true;}
            temp = moveString;
            if(funkEUntenLinks(temp,moveString,player)) {return true;}
            temp = moveString;
            if(funkEUntenRechts(temp,moveString,player)) {return true;}
        }   
        return false;
    }

    public boolean ismovevalidH(String moveString, String player) {
        if(todesblick(moveString, player)){return false;}
        String temp = moveString; 

        temp = oben(moveString,player);
        if(figurImWeg(temp, moveString,player)){}
        else if(richtig(oben(links(temp,player),player),moveString,player)){return true;}
        else if(richtig(oben(rechts(temp,player),player),moveString,player)){return true;}
        
        temp = unten(moveString,player);
        if(figurImWeg(temp, moveString,player)){}
        else if(richtig(unten(links(temp,player),player),moveString,player)){
        	return true;}
        else if(richtig(unten(rechts(temp,player),player),moveString,player)){
        	return true;}
       
        temp = links(moveString,player);
        if(figurImWeg(temp, moveString,player)){}
        else if(richtig(oben(links(temp,player),player),moveString,player)){
        	return true;}
        else if(richtig(unten(links(temp,player),player),moveString,player)){return true;}

        temp = rechts(moveString,player);
        if(figurImWeg(temp, moveString,player)){}
        else if(richtig(oben(rechts(temp,player),player),moveString,player)){
        	return true;}
        else if(richtig(unten(rechts(temp,player),player),moveString,player)){
        	return true;}
                    
        return false;
    }
    
    
    public boolean ismovevalidR(String moveString, String player) {
        if(todesblick(moveString, player)){return false;}
       String temp = moveString;
        for (int i = 0; i < 11; i++){
            temp = oben(temp,player);
            if(temp == "" || figurImWeg(temp,moveString,player)){break;}
            if(richtig(temp, moveString,player)){return true;} }
        temp = moveString;
        for (int i = 0; i < 11; i++){
            temp = unten(temp,player);
            if(temp == "" || figurImWeg(temp,moveString,player)){break;}
            if(richtig(temp, moveString,player)){return true;} }
        temp = moveString;
        for (int i = 0; i < 11; i++){
            temp = links(temp,player);
            if(temp == "" || figurImWeg(temp,moveString,player)){break;}
            if(richtig(temp, moveString,player)){return true;}}
        temp = moveString;
        for (int i = 0; i < 11; i++){
        	  temp = rechts(temp,player);
              if(temp == "" || figurImWeg(temp,moveString,player)){break;}
              if(richtig(temp, moveString,player)){return true;}}
        return false;
    }
    
    public boolean funkCOben(String temp, String moveString, String player) {
    	int count=0;
    	for (int i = 0; i < 11; i++){  
    		temp = oben(temp,player);
    		if(temp == ""){break;}
    		if(figurImWeg(temp,moveString,player)){
    			count++;
    		}
    		if(richtig(temp, moveString,player) && count == 1){
    			return true;
        	}
    	}
        return false;
     }
    public boolean funkCUnten(String temp, String moveString, String player) {
    	int count=0;
    	for (int i = 0; i < 11; i++){  
    		temp = unten(temp,player);
    		if(temp == ""){break;}
    		if(figurImWeg(temp,moveString,player)){
    			count++;
    		}
    		if(richtig(temp, moveString,player) && count == 1){
    			return true;
        	}
    	}
        return false;
     }
    public boolean funkCLinks(String temp, String moveString, String player) {
    	int count=0;
    	for (int i = 0; i < 11; i++){  
    		temp = links(temp,player);
    		if(temp == ""){break;}
    		if(figurImWeg(temp,moveString,player)){
    			count++;
    		}
    		if(richtig(temp, moveString,player) && count == 1){
    			return true;
        	}
    	}
        return false;
     }
    public boolean funkCRechts(String temp, String moveString, String player) {
    	int count=0;
    	for (int i = 0; i < 11; i++){  
    		temp = rechts(temp,player);
    		if(temp == ""){break;}
    		if(figurImWeg(temp,moveString,player)){
    			count++;
    		}
    		if(richtig(temp, moveString,player) && count == 1){
    			return true;
        	}
    	}
        return false;
     }
    
//way too long, selbst nach einspaarung von klammern & kommentare 35 zeilen
    public boolean ismovevalidC(String moveString, String player) {
    	if(todesblick(moveString, player)){return false;}
        char figur = getfigur(String.valueOf( (char)(int) moveString.charAt(3)) + (char)(int) moveString.charAt(4) + (char)((int) moveString.charAt(2))+(char)((int) moveString.charAt(0))+(char)((int) moveString.charAt(1)));
        if(Character.isDigit(figur) || player == "r" && Character.isUpperCase(figur) || player == "s" &&  Character.isLowerCase(figur)){
        	return ismovevalidR(moveString,player);}
        else if(player == "r" && Character.isLowerCase(figur) || player == "s" &&  Character.isUpperCase(figur)){
            String temp = moveString;
            if(funkCOben(temp,moveString,player)) {return true;}
            temp = moveString;
            if(funkCUnten(temp,moveString,player)) {return true;}
            temp = moveString;
            if(funkCLinks(temp,moveString,player)) {return true;}
            temp = moveString;
            if(funkCRechts(temp,moveString,player)) {return true;}
    
            }  
        return false;
    }

    public boolean ismovevalidS(String moveString, String player){
        if(todesblick(moveString, player)){return false;}
    
        if(player == "r" && (int) moveString.charAt(4) <= 52 ){
            if(richtig(oben(moveString, player), moveString,player)){return true;}
        }
        else if(player == "s" 
        		&& (int) moveString.charAt(4) >= 53 ){
            if(richtig(unten(moveString,player), moveString,player)){return true;}
        }
        else if(player == "r"){ //&& (int) moveString.charAt(4) >= 53
            if(richtig(oben(moveString, player), moveString,player)){return true;}
            else if(richtig(links(moveString,player), moveString,player)){return true;}
            else if(richtig(rechts(moveString,player), moveString,player)){return true;}
        }
        else if(player == "s"){ //&& moveString.charAt(4) <= 52
            if(richtig(unten(moveString,player), moveString,player)){return true;}
            else if(richtig(links(moveString,player), moveString,player)){return true;}
            else if(richtig(rechts(moveString,player), moveString,player)){return true;}
        }
        return false;
    }

    public char getfigur(String moveString) {
        if(moveStringValid(moveString) == false){
            return 'f';
        }
        //String str = "rheagaehr/9/1c5c1/s1s1s1s1s/9/9/S1S1S1S1S/1C5C1/9/RHEAGAEHR";    
        String str = realBoard;
        int count = 0;
        int number = (int) moveString.charAt(1) -48; //ascii umrechnen, für zeile die ich will
        int feldwert = ((int) moveString.charAt(0)) - 97; //ascii umrechnen, für reihe die ich will
        String[] str_arr = str.split("/");
        String neustr = str_arr[9-number]; //zeile die ich will
        CharacterIterator it = new StringCharacterIterator(neustr);	
        while (it.current() != CharacterIterator.DONE){
            if(count == feldwert){
                return it.current(); 
            }
            else{       
                if(Character.isLetter(it.current()) == true){
                    count++; 
                }
                else{
                    count+= ((int) it.current()) - 48; 
                }
                it.next();
            }
        }
        return '0';
    }


	public String findeGeneralFeld(char general) {			
		String str = realBoard;		
		int reihe = 9;
		int zeile = 0;		
		CharacterIterator it = new StringCharacterIterator(str);	//do while, falls er ein feld davor abbricht
        while (it.current() != CharacterIterator.DONE && it.current() != general){     	
        	if(it.current() == '/') {
        		reihe--;
        		zeile = 0;
        	}
        	else if(Character.isLetter(it.current())){
    			zeile++;
    		}
    		else if(Character.isDigit(it.current())) {
    		zeile += ((int) it.current()) - 48;
    		}
        	 it.next();
        }
        char ding = (char) (zeile + 97);
        	
		return ""+ding+reihe;
	}
	
	//gibt moveString zurück, wenn ein general schachgesetzt ist, sonst ""
	public String Schach(String generalFeld, String player) {
		String str = realBoard;	
		int reihe = 9;
		int zeile = 0;		
		CharacterIterator it = new StringCharacterIterator(str);	
        while (it.current() != CharacterIterator.DONE){ 
        	if(it.current() == '/') {
        		reihe--;
        		zeile = 0;
        	}
        	else if(Character.isDigit(it.current())) {
        		zeile += ((int) it.current()) - 48;
    		}
        	else if(Character.isLetter(it.current())){
    			zeile++;
    			String feldFigur = ""+(char) (zeile + 96)+reihe; //bei general anschiend +97 und hier +96
        		String moveString = feldFigur +"-"+ generalFeld;
        		if(ismovevalid(moveString,player)) { //&& getfigur(moveString) != 'g' && getfigur(moveString) != 'G'      			
        			return moveString;
        		}
    		}
        	it.next();
        }
		return "";
	}
	
	public boolean istSchachMattRot(String bedrohung,String generalFeld, String player, int i) {
		String[] palastR = new String[] {"d0","d1","d2","e0","e1","e2","f0","f1","f2"};
		String neuePos, boardDavor;
		neuePos = ""+generalFeld+"-"+palastR[i];//&& Schach(palastS[i],player) == ""
		if(ismovevalidG(neuePos, player) ) {// es gibt noch einen platz im palast, wo der general hin kann	
			boardDavor = getBoard();
			setBoard(configureNewBoard(neuePos));
			if(Schach(palastR[i],player) == "") {
				if(getfigur(bedrohung) == 'c' && (int) bedrohung.charAt(0) != (int) neuePos.charAt(0) && getfigur(bedrohung) == 'c' && (int) bedrohung.charAt(1) != (int) neuePos.charAt(1) || getfigur(bedrohung) == 'r' && (int) bedrohung.charAt(0) != (int) neuePos.charAt(0) && getfigur(bedrohung) == 'r' && (int) bedrohung.charAt(1) != (int) neuePos.charAt(1)) {// canone (und rook versteht er nicht mit der linie
					setBoard(boardDavor);
					return false;
				}
			}
			setBoard(boardDavor);
		}
		return true;
	}
	
	public boolean istSchachMattSchwarz(String bedrohung,String generalFeld, String player, int i) {
		String[] palastS = new String[] {"d7","d8","d9","e7","e8","e9","f7","f8","f9"};
		String neuePos, boardDavor;
		neuePos = ""+generalFeld+"-"+palastS[i];
		if(ismovevalidG(neuePos, player)) {// es gibt noch einen platz im palast, wo der general hin kann
			boardDavor = getBoard();
			setBoard(configureNewBoard(neuePos));
			if(Schach(palastS[i],player) == "") {
				if(getfigur(bedrohung) == 'C' && (int) bedrohung.charAt(0) != (int) neuePos.charAt(0) && getfigur(bedrohung) == 'C' && (int) bedrohung.charAt(1) != (int) neuePos.charAt(1) || getfigur(bedrohung) == 'R' && (int) bedrohung.charAt(0) != (int) neuePos.charAt(0) && getfigur(bedrohung) == 'R' && (int) bedrohung.charAt(1) != (int) neuePos.charAt(1)) {
					setBoard(boardDavor);
					return false;
				}
			}
			setBoard(boardDavor);
		}
		return true;
	}
	
	
	// hier prüfen, ob ich die bedrohung von string als notvalid move bekomme
	public boolean istSchachMatt(String bedrohung,String generalFeld, String player) {
		for(int i = 0;i<9;i++) {//für roten general, 8 oder 9??
			if(player == "r" && istSchachMattRot(bedrohung,generalFeld,player,i) == false) {
				return false;
			}
			if(player == "s" && istSchachMattSchwarz(bedrohung,generalFeld,player,i) == false) {
				return false;
			}
		}
		System.out.println("schachmatt!!");
		return true;
	}
	
	public String FeldGefunden(String bedrohung,String figur,String generalFeld, String player,String moveString) {
		String boardDavor, playerAnders;
		if(player == "r") {playerAnders = "s";}
		else {playerAnders = "r";}
		if(ismovevalid(moveString,player)) {//mögliche züge, für jede figur
			boardDavor = getBoard();
			setBoard(configureNewBoard(moveString));
			if(Schach(generalFeld,playerAnders) == "") {
				if(getfigur(bedrohung) == 'c' && (int) bedrohung.charAt(0) != (int) moveString.charAt(0) && getfigur(bedrohung) == 'c' && (int) bedrohung.charAt(1) != (int) moveString.charAt(1) 
						|| getfigur(bedrohung) == 'r' && (int) bedrohung.charAt(0) != (int) moveString.charAt(0) && getfigur(bedrohung) == 'r' && (int) bedrohung.charAt(1) != (int) moveString.charAt(1)
								||getfigur(bedrohung) == 'C' && (int) bedrohung.charAt(0) != (int) moveString.charAt(0) && getfigur(bedrohung) == 'C' && (int) bedrohung.charAt(1) != (int) moveString.charAt(1) 
								|| getfigur(bedrohung) == 'R' && (int) bedrohung.charAt(0) != (int) moveString.charAt(0) && getfigur(bedrohung) == 'R' && (int) bedrohung.charAt(1) != (int) moveString.charAt(1)) {// canone (und rook versteht er nicht mit der linie
					setBoard(boardDavor);
					return moveString;
				}
			}
			setBoard(boardDavor);
		}
		return "";
	}
	
	
	//für eine übergebene figur pruefen, ob immer noch schachmatt ist, wenn man sie auf ein beliebiges anderes feld setzt
	public String jedesFeldPruefen(String bedrohung,String figur,String generalFeld, String player) {
		String str = realBoard;	
		int reihe = 9;
		int zeile = 96;
		CharacterIterator it = new StringCharacterIterator(str);	
        while (it.current() != CharacterIterator.DONE){ 
        	if(it.current() == '/') {reihe--;zeile = 96;}
        	else if(Character.isDigit(it.current())) { //digit ist 9 zusammengefasst!!
        		for(int i =0;i<(it.current()-48);i++) {
        			zeile++;
        			String moveString = ""+figur +"-"+(char) (zeile)+reihe;
        			moveString = FeldGefunden(bedrohung,figur,generalFeld, player,moveString);
        			if(moveString != "") {return moveString;}
        		}
    		}
        	else if(Character.isLetter(it.current())){
    			zeile++;
    			String moveString = ""+figur +"-"+(char) (zeile)+reihe;
    			moveString = FeldGefunden(bedrohung,figur,generalFeld, player,moveString);
    			if(moveString != "") {return moveString;}}
        	it.next();
        }
		return "";
	}
	
	
	public boolean istSchachMattX(String bedrohung,String generalFeld, String player) {
		String str = realBoard;	
		int reihe = 9;
		int zeile = 0;		
		CharacterIterator it = new StringCharacterIterator(str);	
        while (it.current() != CharacterIterator.DONE){ 
        	if(it.current() == '/') {
        		reihe--;
        		zeile = 0;
        	}
        	else if(Character.isDigit(it.current())) {
        		zeile += ((int) it.current()) - 48;
    		}
        	else if(Character.isLetter(it.current())){
    			zeile++;
    			String figur = ""+(char) (zeile + 96)+reihe;
    			if(jedesFeldPruefen(bedrohung,figur,generalFeld,player) != "") {
    				return false;
    			}
    		}
        	it.next();
        }
        System.out.println("schachmattX!!");
		return true;
	}

	
	
}
